const tipoInput = document.querySelector('#input-tipo');
const montoInput = document.querySelector('#input-monto');
const guardarBtn = document.querySelector('#btn-guardar');
const gastosList = document.querySelector('#list-gastos');
const totalOutput = document.querySelector('#ouput-total');

let total = 0;


guardarBtn.addEventListener('click', ()=>{

    const tipo = tipoInput.value;
    const monto = montoInput.value;

    if (tipo.trim().length > 0 && monto.trim().length > 0 && monto > 0){
        const newItem = document.createElement('ion-item');
        newItem.textContent = tipo + ': $' + monto;
        gastosList.appendChild(newItem);

        total += +monto;
        totalOutput.textContent = total;
        clear();
        //console.error();
        //console.warn();
        //console.table();
    }
    else{
        //console.error('Valores invalidos');
        
        alertController.create({
            message: 'Llene los campos correctamente',
            header: 'Valores invalidos',
            buttons: ['Ok']
        }).then(alertElement => {
            alertElement.present();
        });   
    }

});